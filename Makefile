dir=${CURDIR}

# Load environment vars
include ${PWD}/etc/.env.dist
export $(shell sed 's/=.*//' ${PWD}/etc/.env.dist)

MAKEFILE := $(abspath $(lastword $(MAKEFILE_LIST)))


# COLORS
GREEN  := $(shell tput -Txterm setaf 2)
YELLOW := $(shell tput -Txterm setaf 3)
WHITE  := $(shell tput -Txterm setaf 7)
VIOLET := $(shell tput -Txterm setaf 5)
BlUE := $(shell tput -Txterm setaf 4)
RESET  := $(shell tput -Txterm sgr0)
NEWLINE := $(shell printf '\010')

symfony_user=-u www-data
project=-p ${PROJECT_NAME}
service=${COMPOSE_PROJECT_NAME}:latest
openssl_bin:=$(shell which openssl)
interactive:=$(shell [ -t 0 ] && echo 1)
PROJECT_URL=https://${VIRTUAL_HOST}
COMPOSER_HOME=$(HOME)/.composer
USER_NAME ?= $(shell id -u -n)
NPM_CACHE=$(HOME)/.npm

UID ?= $(shell id -u)
GID ?= $(shell id -g)
PWD ?= $(shell pwd)
ifneq ($(interactive),1)
	optionT=-T
endif
ifeq ($(GITLAB_CI),1)
	# Determine additional params for phpunit in order to generate coverage badge on GitLabCI side
	phpunitOptions=--coverage-text --colors=never
endif

include etc/make/command.mk
