<?php
declare(strict_types=1);

namespace App\Controller;



use App\Form\CheaperSupplierType;
use App\Service\CheaperService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/", name="index")
 */
class HomeController extends AbstractController
{
    public function __invoke(Request $request)
    {
        $form = $this->createForm(CheaperSupplierType::class);
        $form->handleRequest($request);

        $response = null;
        if ($form->isSubmitted() && $form->isValid()) {
                $response = CheaperService::cheaper($form->getData());
                unset($form);
                $form = $this->createForm(CheaperSupplierType::class);
        }
        return $this->render('index.html.twig',[
          'form_supplier' => $form->createView(),
            'result' => $response
        ]);
    }
}
