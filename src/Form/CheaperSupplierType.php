<?php

declare(strict_types=1);

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormBuilderInterface;


class CheaperSupplierType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('dentalFloss',IntegerType::class, [
                'label' => false,
                'required' => true,
                'attr' => ['name' => 'form-cheaper-supplier[dentalFloss]',
                    'id' => 'dentalFloss',
                'value' => 0],
            ])
            ->add('ibuprofen',IntegerType::class, [
                'label' => false,
                'required' => true,
                'attr' => ['name' => 'form-cheaper-supplier[ibuprofen]',
                    'id' => 'ibuprofen',
                    'value' => 0],
            ])
       ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'attr' => ['id' => 'form-cheaper-supplier'],
            'method' => 'POST',
        ]);
    }
}

