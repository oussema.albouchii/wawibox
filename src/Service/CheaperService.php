<?php

declare(strict_types=1);

namespace App\Service;

class CheaperService
{
    private static $suppliers = [
      'supplierA' => [
          //unit price array
          'dentallFloss' => [
              1 => 9,
              20 => 160
          ],
          //unit price array
          'ibuprofen' => [
              1 => 5,
              10 => 48
          ]
      ]  ,
        'supplierB' => [
            //unit price array
            'dentallFloss' => [
                1 => 8,
                10 => 71
            ],
            //unit price array
            'ibuprofen' => [
                1 => 6,
                5 => 25,
                100 => 410
            ]
        ]
    ];
   public static function cheaper(array $data){
       $result =[];
       foreach (self::$suppliers as $supplierKey => $supplierContent){
           $dentalFlossSomme = self::somme($supplierContent['dentallFloss'], $data['dentalFloss']);
           $ibuprofenSomme = self::somme($supplierContent['ibuprofen'], $data['ibuprofen']);
           $result[$supplierKey] = $dentalFlossSomme + $ibuprofenSomme;
       }
       $cheapersupplier = array_search(min($result), $result);
       return $cheapersupplier." is cheaper ".$result[$cheapersupplier]." €";
   }

   public static function somme(array $array, int $amount){
       krsort($array);
       $somme = 0;
       foreach ($array as $key => $value){
           if($key <= $amount){
               $somme += intdiv($amount, $key) * $value;
               $amount = $amount % $key;
           }
       }
         return $somme;
   }
}
